package ru.t1consulting.nkolesnik.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/edit/*")
public class TaskEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Task task = TaskRepository.getInstance().findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statuses", Status.values());
        req.setAttribute("projects", ProjectRepository.getInstance().findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final String taskId = req.getParameter("id");
        @NotNull final String projectId = req.getParameter("projectId");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final String startDateValue = req.getParameter("startDate");
        @NotNull final String endDateValue = req.getParameter("endDate");

        Task task = new Task(
                taskId,
                name,
                description,
                DateUtil.toDate(startDateValue),
                DateUtil.toDate(endDateValue),
                status,
                projectId.isEmpty() ? null : projectId
        );
        TaskRepository.getInstance().save(task);
        resp.sendRedirect("/tasks");
    }

}
