package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("TASK_1"));
        add(new Task("TASK_2"));
        add(new Task("TASK_3"));
        add(new Task("TASK_4"));
    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    private void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void save(@NotNull final Task task) {
        add(task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}
