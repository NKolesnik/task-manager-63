package ru.t1consulting.nkolesnik.tm.servlet.task;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/task/delete/*")
public class TaskDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String taskId = req.getParameter("id");
        TaskRepository.getInstance().removeById(taskId);
        resp.sendRedirect("/tasks");
    }

}
