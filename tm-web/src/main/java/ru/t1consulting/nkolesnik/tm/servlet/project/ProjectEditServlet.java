package ru.t1consulting.nkolesnik.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.util.DateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String projectId = req.getParameter("id");
        @NotNull final Project project = ProjectRepository.getInstance().findById(projectId);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final String projectId = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final Status status = Status.valueOf(req.getParameter("status"));
        @NotNull final String startDateValue = req.getParameter("startDate");
        @NotNull final String endDateValue = req.getParameter("endDate");

        @NotNull final Project project = new Project(
                projectId,
                name,
                description,
                DateUtil.toDate(startDateValue),
                DateUtil.toDate(endDateValue),
                status
        );

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
