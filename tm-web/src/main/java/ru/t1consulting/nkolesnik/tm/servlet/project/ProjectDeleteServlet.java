package ru.t1consulting.nkolesnik.tm.servlet.project;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String projectId = req.getParameter("id");
        ProjectRepository.getInstance().removeById(projectId);
        resp.sendRedirect("/projects");
    }

}
