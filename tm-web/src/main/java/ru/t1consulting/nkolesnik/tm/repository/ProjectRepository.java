package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("PROJ_1"));
        add(new Project("PROJ_2"));
        add(new Project("PROJ_3"));
        add(new Project("PROJ_4"));
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis()));
    }

    public void save(@NotNull final Project project) {
        add(project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
