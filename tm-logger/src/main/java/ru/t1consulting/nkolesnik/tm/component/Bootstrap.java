package ru.t1consulting.nkolesnik.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1consulting.nkolesnik.tm.api.service.IReceiverService;
import ru.t1consulting.nkolesnik.tm.listener.LoggerListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private LoggerListener loggerListener;

    public void run() {
        receiverService.receive(loggerListener);
    }

}
